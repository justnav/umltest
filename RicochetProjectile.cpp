// Fill out your copyright notice in the Description page of Project Settings.

#include "RicochetProjectile.h"
#include "Engine/GameEngine.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "HEXCharacter.h"
#include "MeleeAI.h"


ARicochetProjectile::ARicochetProjectile()
{
	PrimaryActorTick.bCanEverTick = true;
	ProjectileMovementComponent->OnProjectileStop.AddDynamic(this, &ARicochetProjectile::OnStop);
}

void ARicochetProjectile::Tick(float DeltaTime)
{
	if (ProjectileHit) CalculateDirection(HitActor);
	if (ProjectileStop) ApplyMovement();
}

void ARicochetProjectile::BeginPlay()
{
	Super::BeginPlay();
	FCQParams = FCollisionQueryParams::DefaultQueryParam;
	FCQParams.AddIgnoredActor(this);
}

void ARicochetProjectile::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	ProjectileHealth--;//REDUCE THE PROJECTILE'S HEALTH. A.K.A NUMBER OF FUTURE JUMPS POSSIBLE
	//Prints the health of the projectile
	FString HealthAsString = FString::FromInt(ProjectileHealth);
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, HealthAsString);

	//Destroys itself when it has hit x number of targets
	if (ProjectileHealth <= 0)
	{
		Destroy();
	}

	//Destroys itself if it hits a player
	if (OtherActor->IsA(AHEXCharacter::StaticClass()))
	{
		Destroy();
	}
	
	HitActor = OtherActor;
	CalculateDirection(HitActor);
	ProjectileHit = true;
}
//void ARicochetProjectile::Delay_Implementation(FVector TargetDir) 
//{
//}

void ARicochetProjectile::OnStop(const FHitResult &Hit)//Called when the projectile hits a body and is forced to stop
{
	this->ProjectileMovementComponent->SetUpdatedComponent(SphereCollisionComponent);
	ApplyMovement();
	ProjectileStop = true;
}

FHitResult ARicochetProjectile::GetNextLoc()
{
	UWorld* World = this->GetWorld();
	FHitResult HitTarget;

	//SHOULD RETURN NULL CANT FIGURE WHY IT WONT LET ME
	//if (World == nullptr)return HitTarget;
	
	DrawDebugLine(World, GetActorLocation(), TargetLocation, FColor::Red, true, 10.0f);

	World->LineTraceSingleByChannel(HitTarget, GetActorLocation(), TargetLocation, ECollisionChannel::ECC_Pawn, FCQParams);

	if (HitTarget.GetActor() != NULL)
	{
		FString name = HitTarget.GetActor()->GetName();
		//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Blue,"Ray Cast Hit");
	//	GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Blue, name);
		return HitTarget;
	}
	//SHOULD RETURN NULL CANT FIGURE WHY IT WONT LET ME
	return HitTarget;
	GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Green, "Target Null");
}

void ARicochetProjectile::CalculateDirection(AActor* OtherActor)
{
	ProjectileHit = false;

	if (OtherActor->IsA(AMeleeAI::StaticClass()))//IF HIT ACTOR IS AN AI
	{

		//CASTS OVERLAP SPHERE

		CastOverlapShere();

		FHitResult Target = GetNextLoc();

		//if (Target == nullptr)return;
			   
		//CALCULATES NEW ROTATION

		FRotator NewRot;
		FVector Dir;

		NewRot.Roll = this->GetActorRotation().Roll;
		NewRot.Pitch = this->GetActorRotation().Pitch;

		Dir = Target.Location;

		Dir.Z = 0;

		NewRot.Yaw = FRotationMatrix::MakeFromX(Dir - GetActorLocation()).Rotator().Yaw;

		this->SetActorRotation(NewRot, ETeleportType::None);//SETS THE ROTATION OF THE ACTOR

		//PRINTS TARGET NAME
	//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Green, "Hit Target");
	//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Green, OtherActor->GetName());
		//DESTROYS TARGET
		OtherActor->Destroy();
	}
	else
	{
		Destroy();
	}

}

void ARicochetProjectile::CastOverlapShere()
{
	TArray<TEnumAsByte<EObjectTypeQuery>> Types;
	TArray<AActor*> ActorsToIgnore;// STORES ACTORS TO BE IGNORED BY SPHERE CAST
	TArray<AActor*> OutActors;//STORES THE ACTORS IN THE SPHERE OVERLAP
	TArray<AActor*> FloorArray;//STORES FLOOR ACTOR

	UGameplayStatics::GetAllActorsWithTag(GetWorld(), "Floor", FloorArray);//GETS ALL INSTANCES WITH FLOOR TAG (WILL NEED SIMILAR STATEMENTS FOR OTHER ENVIRONMENT MESHES)

	ActorsToIgnore.Add(GetOwner());
	ActorsToIgnore.Add(FloorArray[0]);

	bool isOverlap = UKismetSystemLibrary::SphereOverlapActors(GetWorld(), GetActorLocation(), Radius, Types, AMeleeAI::StaticClass(), ActorsToIgnore, OutActors);
	//DrawDebugSphere(GetWorld(), GetActorLocation(), Radius, 26, FColor(181, 0, 0), true, -1);

	//isOverlap IS TRUE IF ANY AI ARE IN THE SPHERE OVERLAP RADIUS
	if (isOverlap)
	{
		TargetLocation = FindClosest(OutActors)->GetActorLocation(); //SETS LOCATION OF THE NEXT TARGET

		FString Name = FindClosest(OutActors)->GetName();
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, "Closest Target");
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, Name);
	}
	else
	{
		Destroy();
	}
}

void ARicochetProjectile::ApplyMovement()
{
	ProjectileStop = false;
	this->ProjectileMovementComponent->Velocity = this->GetActorForwardVector() * ReboundSpeed;
	this->ProjectileMovementComponent->UpdateComponentVelocity();
}

AActor* ARicochetProjectile::FindClosest(TArray<AActor*> List)
{
	AActor* ClosestActor = List[0];
	AActor* NextActor;
	float ClosestDist;
	float NextDist;
	int ListLen = List.Num();

	if (ListLen == 0) return ClosestActor;

	for (int i = 0; i < (ListLen - 1); i++)
	{
		ClosestActor = List[i];
		NextActor = List[i + 1];
		ClosestDist = FVector::Dist(this->GetActorLocation(), ClosestActor->GetActorLocation());
		NextDist = FVector::Dist(this->GetActorLocation(), NextActor->GetActorLocation());
		if (ClosestDist > NextDist)
		{
			ClosestActor = NextActor;
		}
	}
	return ClosestActor;
}
