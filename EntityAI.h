// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "EntityAI.generated.h"

/**
 * 
 */
UCLASS()
class HEX_API AEntityAI : public AAIController
{
	GENERATED_BODY()
public:
		UPROPERTY(transient, EditAnywhere)
			class UBlackboardComponent *BlackboardComp;
		
	UPROPERTY(transient)
		class UBehaviorTreeComponent *BehaviorComp;

public:

	AEntityAI();

	virtual void Possess(APawn *InPawn) override;

	uint8 EntityKeyID;
};
