// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HEXCharacter.h"
#include "Components/SphereComponent.h"
#include "RespawnOrb.generated.h"

UCLASS()
class HEX_API ARespawnOrb : public AActor
{
	GENERATED_BODY()
	
public:	
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Projectile")
		USphereComponent* SphereCollisionComponent;

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	//virtual void Tick(float DeltaTime) override;

	// Sets default values for this actor's properties
	ARespawnOrb();

};
