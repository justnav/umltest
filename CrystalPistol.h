// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Components/SplineComponent.h"
#include "CrystalPistol.generated.h"

UCLASS()
class HEX_API ACrystalPistol : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACrystalPistol();
	
	UPROPERTY(BlueprintReadWrite)
	USplineComponent* SplineComponent;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* CPMesh;

	float fMoveSpeed = 0.3f;
	float fSpeedIncrease = 1.0f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	float fDistanceAlongSpline = 0.f;



public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
