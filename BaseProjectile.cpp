// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseProjectile.h"
#include "HEXCharacter.h"
#include "ElfCharacter.h"
#include "Engine/GameEngine.h"

// Sets default values
ABaseProjectile::ABaseProjectile()
{
 	
	//PrimaryActorTick.bCanEverTick = true;

	SphereCollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollisionComponent"));

	RootComponent = SphereCollisionComponent;

	SphereCollisionComponent->InitSphereRadius(40.0f);
	SphereCollisionComponent->SetCollisionProfileName("Destructible");
	SphereCollisionComponent->OnComponentHit.AddDynamic(this, &ABaseProjectile::OnHit);
	
	SphereCollisionComponent->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	SphereCollisionComponent->CanCharacterStepUpOn = ECB_No;

	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
	ProjectileMovementComponent->UpdatedComponent = SphereCollisionComponent;
	ProjectileMovementComponent->InitialSpeed = 2000.f;
	ProjectileMovementComponent->MaxSpeed = 2000.f;
	ProjectileMovementComponent->ProjectileGravityScale = 0.f;
	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	ProjectileMovementComponent->bShouldBounce = false;

	InitialLifeSpan = 5.f;
}
  
void ABaseProjectile::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{/*
	//TO BE CHANGED TO DESTROY ITSELF IF IT HITS ANYTHING THAT's NOT AN AI
	if (OtherActor->IsA(AHEXCharacter::StaticClass()))
	{
		AHEXCharacter* Player = Cast<AHEXCharacter>(OtherActor);
		Player->ApplyDamage(Damage);
		Destroy();
	}
	else if (OtherActor->IsA(AElfCharacter::StaticClass()))
	{
		AElfCharacter* Player = Cast<AElfCharacter>(OtherActor);
		Player->ApplyDamage(Damage);
		Destroy();
	}
	else if (OtherActor->IsA(AAI_Parent::StaticClass()))
	{
		AAI_Parent* Enemy = Cast<AAI_Parent>(OtherActor);
		Enemy->ApplyDamage(Damage);
		//Destroy();
	}
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComponent != NULL) && OtherComponent->IsSimulatingPhysics())
	{	
		OtherActor->Destroy();

		Destroy();
	}
	//Destroy();*/
}

void ABaseProjectile::InitVelocity(const FVector& Direction)
{
	this->ProjectileMovementComponent->Velocity = Direction * this->ProjectileMovementComponent->InitialSpeed;
}

