// Fill out your copyright notice in the Description page of Project Settings.

#include "AI_Parent.h"
#include "Perception/PawnSensingComponent.h"

// Sets default values
AAI_Parent::AAI_Parent()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PawnSensingComponent = CreateAbstractDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComponent"));

	//Sets pawnsensing values
	PawnSensingComponent->OnSeePawn.AddDynamic(this, &AAI_Parent::OnPawnSeen);
	PawnSensingComponent->SetPeripheralVisionAngle(45.0f);
	PawnSensingComponent->SightRadius = 1000.0f;
	PawnSensingComponent->bHearNoises = false;
	PawnSensingComponent->bOnlySensePlayers = true;
}

// Called when the game starts or when spawned
void AAI_Parent::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAI_Parent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AAI_Parent::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

											
void AAI_Parent::OnPawnSeen(APawn * SeenPawn)//Draws a debug sphere at player location when the player eneters it's vision cone 
{

	if (SeenPawn == nullptr)
	{
		return;
	}
	//DrawDebugSphere(GetWorld(), SeenPawn->GetActorLocation(), 32.0f, 12, FColor::Red, false, 10.0f);
}
void AAI_Parent::ApplyDamage(float Damage)
{
	this->CurrentHealth -= Damage;

	if (this->CurrentHealth <= 0)
	{
		this->Destroy();
	}
}