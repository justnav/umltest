// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "DrawDebugHelpers.h"
#include "ElfTarget.h"
#include "AI_Parent.h"
#include "Runtime/Engine/Classes/GameFramework/ForceFeedbackEffect.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "WizardCharacter.generated.h"

UCLASS(config = Game)
class HEX_API AWizardCharacter : public ACharacter
{
	GENERATED_BODY()

		///** Camera boom positioning the camera behind the character */
		//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		//class USpringArmComponent* CameraBoom;

		///** Follow camera */
		//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		//class UCameraComponent* FollowCamera;

public:
	AWizardCharacter();

	float InputX;
	float InputY;
	float DefaultWalkSpeed;
	bool isAlive = true;
	bool ChargeActive = false;
	bool isAutoActive = false;
	bool AutoCD = false;
	bool isWizardTargeting = false;
	FVector TargetLocation;
	FRotator DefaultRot;
	FVector DefaultPos;
	FTimerHandle CooldownHandler;
	AElfTarget* ChargeTarget = nullptr;


	//UPROPERTY(EditAnywhere)
		//UStaticMeshComponent* LaserAimComponent;

	UPROPERTY(BlueprintReadWrite)
		int32 PlayerIndex = 0;
	UPROPERTY()
		bool ToBeRespawned = false;
	UPROPERTY(BlueprintReadOnly, Category = Health)
		float MaxHealth = 100;
	UPROPERTY(BlueprintReadOnly, Category = Health)
		float CurrentHealth = 100;
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	//Projectile for auto attack
	UPROPERTY(EditAnywhere, Category = Attacks)
		TSubclassOf<class ABaseProjectile> AutoAttackProjectile;

	//Projectile for Charged attack
	UPROPERTY(EditAnywhere, Category = Attacks)
		TSubclassOf<class AElfTarget> ChargeTargetClass;

	UPROPERTY(EditAnywhere, Category = Attacks)
		TSubclassOf<class AHealingPond> HealingPondClass;

	UPROPERTY(EditAnywhere, Category = Feedback_Target)
		UForceFeedbackEffect* FeedbackEffect;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Targeting")
		UStaticMeshComponent* LaserAimComponent;

	UFUNCTION(BlueprintCallable)
		void ApplyDamage(float Damage);


protected:

	virtual void Tick(float DeltaTime) override;

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	void ShootUp(float Rate);
	void ShootRight(float Rate);

	void ApplyRotation();

	void AutoAttackCooldown();
	void OnTimerEnd();

	void ShootAuto();
	//void ShootCharged();
	void EnableAuto();
	void DisableAuto();
	void ActivateCharged();

	void Respawn();
	//void CastOverlapShere();
	void WizardCharge();
	void WizardChargeEnd();

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	///** Returns CameraBoom subobject **/
	//FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	///** Returns FollowCamera subobject **/
	//FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

};
