// Fill out your copyright notice in the Description page of Project Settings.

#include "RangedAI.h"

// Sets default values
ARangedAI::ARangedAI()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARangedAI::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARangedAI::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ARangedAI::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ARangedAI::OnPawnSeen(APawn * SeenPawn)//Draws a debug sphere at player location when the player eneters it's vision cone 
{

	if (SeenPawn == nullptr)
	{
		return;
	}


	//DrawDebugSphere(GetWorld(), SeenPawn->GetActorLocation(), 32.0f, 12, FColor::Red, false, 10.0f);
}
void ARangedAI::ShootProjectile()
{
	FVector SpawnLocation = GetActorRotation().Vector() * 100.f;
	SpawnLocation += GetActorLocation();
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = Instigator;
	ABaseProjectile* BProjectile = GetWorld()->SpawnActor<ABaseProjectile>(RangedProjectile, SpawnLocation, GetActorRotation(), SpawnInfo);
	BProjectile->InitVelocity(GetActorRotation().Vector());
}