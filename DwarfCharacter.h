// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "DrawDebugHelpers.h"
#include "AI_Parent.h"
#include "Runtime/Engine/Classes/GameFramework/ForceFeedbackEffect.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "DwarfCharacter.generated.h"


UCLASS(config = Game)
class ADwarfCharacter : public ACharacter
{
	GENERATED_BODY()

		///** Camera boom positioning the camera behind the character */
		//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		//class USpringArmComponent* CameraBoom;

		///** Follow camera */
		//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		//class UCameraComponent* FollowCamera;

public:
	ADwarfCharacter();

	float InputX;
	float InputY;
	float DefaultWalkSpeed;
	bool isAlive = true;
	bool ChargeActive = false;
	bool isAutoActive = false;
	bool AutoCD = false;
	bool isElfTargeting = false;
	FVector TargetLocation;
	FRotator DefaultRot;
	FVector DefaultPos;
	FTimerHandle CooldownHandler;


	//UPROPERTY(EditAnywhere)
		//UStaticMeshComponent* LaserAimComponent;
	UPROPERTY(BlueprintReadWrite)
		int32 PlayerIndex = 0;
	UPROPERTY()
		bool ToBeRespawned = false;
	UPROPERTY(BlueprintReadOnly, Category = Health)
		float MaxHealth = 100;
	UPROPERTY(BlueprintReadOnly, Category = Health)
		float CurrentHealth = 100;
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	//Projectile for auto attack
	UPROPERTY(EditAnywhere, Category = Projectiles)
		TSubclassOf<class ABaseProjectile> AutoAttackProjectile;

	UPROPERTY(EditAnywhere, Category = Feedback_Target)
		UForceFeedbackEffect* FeedbackEffect;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Projectile")
		UStaticMeshComponent* LaserAimComponent;

	UFUNCTION(BlueprintCallable)
		void ApplyDamage(float Damage);


protected:

	virtual void Tick(float DeltaTime) override;

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	void ShootUp(float Rate);
	void ShootRight(float Rate);

	void ApplyRotation();

	void AutoAttackCooldown();
	void OnTimerEnd();

	void ShootAuto();
	//void ShootCharged();
	void EnableAuto();
	void DisableAuto();
	void ActivateCharged();

	void Respawn();
	//void CastOverlapShere();
	//void ElfCharge();
	//void ElfChargeEnd();

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	///** Returns CameraBoom subobject **/
	//FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	///** Returns FollowCamera subobject **/
	//FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};

