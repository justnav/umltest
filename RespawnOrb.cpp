// Fill out your copyright notice in the Description page of Project Settings.

#include "RespawnOrb.h"

// Sets default values
ARespawnOrb::ARespawnOrb()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;
	SphereCollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollisionComponent"));
}

// Called when the game starts or when spawned
void ARespawnOrb::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
//void ARespawnOrb::Tick(float DeltaTime)
//{
//	Super::Tick(DeltaTime);
//}


