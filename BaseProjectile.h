// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "AI_Parent.h"
#include "Components/SphereComponent.h"
#include "BaseProjectile.generated.h"

class UProjectileMovementComponent;
class USphereComponent;

UCLASS()
class HEX_API ABaseProjectile : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Damage")
		float Damage = 10;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Projectile")
		USphereComponent* SphereCollisionComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Projectile Movement")
		UProjectileMovementComponent* ProjectileMovementComponent;

public:	

	ABaseProjectile();

	UFUNCTION()
		virtual void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
		void InitVelocity(const FVector& Direction);

	USphereComponent* GetCollisionComponent() const { return SphereCollisionComponent; }

	UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovementComponent; }
};
