// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Perception/PawnSensingComponent.h"
#include "AI_Parent.h"
#include "MeleeAI.generated.h"

UCLASS()
class HEX_API AMeleeAI : public AAI_Parent
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMeleeAI();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
