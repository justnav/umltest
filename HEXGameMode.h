// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GameFramework/PlayerStart.h"
#include "HEXGameMode.generated.h"
UCLASS(minimalapi)
class AHEXGameMode : public AGameModeBase
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;
	UFUNCTION()
		void  GetPlayerStartPoints();
	UFUNCTION()
		void SpawnPlayers();
public:
	AHEXGameMode();
	TArray<AActor*> PlayerStartArray;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerStart)
	TArray<APawn*> Players;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = NumPlayers)
	int32 MaxPlayerCount;
	int32 Index;
	TSubclassOf<APawn> PlayerSpawn;
	TSubclassOf<APawn> PlayerSecondSpawn;
	TSubclassOf<APawn> PlayerThirdSpawn;
	TSubclassOf<APawn> PlayerFourthSpawn;
};


