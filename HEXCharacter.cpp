// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "HEXCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "TimerManager.h"
#include "HEXGameMode.h"
#include "Haptics/HapticFeedbackEffect_Base.h"
#include "Runtime/Engine/Classes/GameFramework/Controller.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"

#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/GameEngine.h"
#include "BaseProjectile.h"

AHEXCharacter::AHEXCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;


	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	//// Create a camera boom (pulls in towards the player if there is a collision)
	//CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	//CameraBoom->SetupAttachment(RootComponent);
	//CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	//CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	//// Create a follow camera
	//FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	//FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	//FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	LaserAimComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("LaserAimComponent"));
	LaserAimComponent->AttachTo(RootComponent);
	LaserAimComponent->SetVisibility(false);

	DefaultWalkSpeed = this->GetCharacterMovement()->MaxWalkSpeed;
}

void AHEXCharacter::Tick(float DeltaTime)
{
	ApplyRotation();
	if (isAutoActive) ShootAuto();
}
//////////////////////////////////////////////////////////////////////////
// Input

void AHEXCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Respawn", IE_Pressed, this, &AHEXCharacter::Respawn);;

	PlayerInputComponent->BindAxis("MoveForward", this, &AHEXCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AHEXCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	//PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	//PlayerInputComponent->BindAxis("TurnRate", this, &AHEXCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("ShootUp", this, &AHEXCharacter::ShootUp);
	PlayerInputComponent->BindAxis("ShootRight", this, &AHEXCharacter::ShootRight);

	PlayerInputComponent->BindAction("ShootAuto", IE_Pressed, this, &AHEXCharacter::EnableAuto);
	PlayerInputComponent->BindAction("ShootAuto", IE_Released, this, &AHEXCharacter::DisableAuto);
	/*PlayerInputComponent->BindAction("ShootCharged", IE_Pressed, this, &AHEXCharacter::ActivateCharged);
	PlayerInputComponent->BindAction("ShootCharged", IE_Released, this, &AHEXCharacter::ShootCharged);*/


}

void AHEXCharacter::ShootUp(float Rate)
{
	//calculate delta for this frame from the rate information
	//AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
	InputX = Rate;
}

void AHEXCharacter::ShootRight(float Rate)
{
	// calculate delta for this frame from the rate information
	//AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
	InputY = Rate;
}

void AHEXCharacter::ApplyRotation()
{
	//Right Analog fire
	FVector direction(InputX, InputY, 0.0f);
	direction.X *= -1;

	if (direction.Size() > 0.25f)
	{
		//this->SetActorRotation(direction.Rotation());
		LaserAimComponent->SetVisibility(true);
		this->SetActorRotation(FMath::Lerp(FQuat(GetActorRotation()), FQuat(direction.Rotation()), 0.1f));
		//UE_LOG(LogTemp, Warning, TEXT("Rotating"));
	}
	else
	{
		LaserAimComponent->SetVisibility(false);
	}
}

void AHEXCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		if (isElfTargeting)//REMOVE ONCE ELF SUBCLASS IS MADE
		{
			ChargeTarget->AddMovementInput(Direction, Value, true);
		}
		else AddMovementInput(Direction, Value);
	}
}

void AHEXCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		if (isElfTargeting)//REMOVE ONCE ELF SUBCLASS IS MADE
		{
			ChargeTarget->AddMovementInput(Direction, Value, true);
			//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "RightLeft");
		}
		else AddMovementInput(Direction, Value);
	}
}

void AHEXCharacter::ApplyDamage(float Damage)
{
	this->CurrentHealth -= Damage;

	if (this->CurrentHealth <= 0)
	{
		isAlive = false;
		DefaultPos = GetActorLocation();
		DefaultRot = GetActorRotation();
		DefaultScale = GetActorScale();
		//GetCharacterMovement()->GravityScale = 0;	
		//GetMesh()->BodyInstance.SetObjectType(ECC_PhysicsBody);
		//GetMesh()->BodyInstance.SetResponseToChannel(ECC_WorldStatic, ECR_Block);
		GetMovementComponent()->StopMovementImmediately();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		GetMesh()->SetCollisionProfileName("Ragdoll");
		GetMesh()->BodyInstance.SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics, true);
		//GetMesh()->SetAllBodiesSimulatePhysics(true);
		GetMesh()->SetSimulatePhysics(true);
		//GetMesh()->WakeAllRigidBodies();
		//GetMesh()->bBlendPhysics = true;
	}
	if (this->CurrentHealth > MaxHealth)
	{
		this->CurrentHealth = MaxHealth;
	}
}

void AHEXCharacter::ShootAuto()
{
	if (!ChargeActive)
	{
		if (!AutoCD)
		{
			AutoCD = true;
			FVector SpawnLocation = GetActorRotation().Vector() * 100.f;
			SpawnLocation += GetActorLocation();
			FActorSpawnParameters SpawnInfo;
			SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnInfo.Owner = this;
			SpawnInfo.Instigator = Instigator;
			ABaseProjectile* BProjectile = GetWorld()->SpawnActor<ABaseProjectile>(AutoAttackProjectile, SpawnLocation, GetActorRotation(), SpawnInfo);
			BProjectile->InitVelocity(GetActorRotation().Vector());

			AutoAttackCooldown();
		}
		//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, "OnCooldown");
	}
}

void AHEXCharacter::EnableAuto()
{
	isAutoActive = true;
}

void AHEXCharacter::DisableAuto()
{
	isAutoActive = false;
}

void AHEXCharacter::AutoAttackCooldown()
{
	GetWorldTimerManager().SetTimer(CooldownHandler, this, &AHEXCharacter::OnTimerEnd, 0.4f, false);
}

void AHEXCharacter::OnTimerEnd()
{
	AutoCD = false;
	//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Green, "OffCooldown");
}


void AHEXCharacter::ActivateCharged()
{
	ChargeActive = true;

	FLatentActionInfo latentInfo;
	latentInfo.CallbackTarget = this;
	UGameplayStatics::GetPlayerController(GetWorld(), PlayerIndex)->ClientPlayForceFeedback(FeedbackEffect, false, "Charge");
}

/*void AHEXCharacter::ShootCharged()
{
	ChargeActive = false;
	FVector SpawnLocation = GetActorRotation().Vector() * 100.f;
	SpawnLocation += GetActorLocation();
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = Instigator;
	ABaseProjectile* BProjectile = GetWorld()->SpawnActor<ABaseProjectile>(ChargeAttackProjectile, SpawnLocation, GetActorRotation(), SpawnInfo);
	BProjectile->InitVelocity(GetActorRotation().Vector());

	UGameplayStatics::GetPlayerController(GetWorld(), PlayerIndex)->ClientStopForceFeedback(FeedbackEffect, "Charge");
}*/

void AHEXCharacter::Respawn()
{
	TArray<TEnumAsByte<EObjectTypeQuery>> Types;
	TArray<AActor*> ActorsToIgnore;// STORES ACTORS TO BE IGNORED BY SPHERE CAST
	TArray<AActor*> OutActors;//STORES THE ACTORS IN THE SPHERE OVERLAP
	TArray<AActor*> FloorArray;//STORES FLOOR ACTOR

	UGameplayStatics::GetAllActorsWithTag(GetWorld(), "Floor", FloorArray);//GETS ALL INSTANCES WITH FLOOR TAG (WILL NEED SIMILAR STATEMENTS FOR OTHER ENVIRONMENT MESHES)

	ActorsToIgnore.Add(GetOwner());
	ActorsToIgnore.Add(FloorArray[0]);

	bool isOverlap = UKismetSystemLibrary::SphereOverlapActors(GetWorld(), GetActorLocation(), 200, Types, AHEXCharacter::StaticClass(), ActorsToIgnore, OutActors);
	DrawDebugSphere(GetWorld(), GetActorLocation(), 200, 26, FColor::Green, true, -1);

	if (isOverlap)
	{
		for (AActor* Actor : OutActors)
		{
			AHEXCharacter* PlayerChar = Cast<AHEXCharacter>(Actor);
			if (!(PlayerChar->isAlive))
			{
				PlayerChar->GetMesh()->SetSimulatePhysics(false);
				PlayerChar->GetMesh()->BodyInstance.SetCollisionEnabled(ECollisionEnabled::NoCollision, true);
				PlayerChar->GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
				PlayerChar->GetMesh()->AttachTo(PlayerChar->RootComponent);
				PlayerChar->GetMesh()->SetRelativeLocation(FVector(DefaultPos.X, DefaultPos.Y, (DefaultPos.Z - 94)));
				//PlayerChar->GetMesh()->SetRelativeRotation(DefaultRot);
				PlayerChar->GetMesh()->SetRelativeRotation(PlayerChar->GetActorForwardVector().Rotation().Add(0,-90,0));
				PlayerChar->isAlive = true;
				PlayerChar->CurrentHealth = PlayerChar->MaxHealth;
			}
		}
	}
}

/*
void AHEXCharacter::CastOverlapShere()
{
	TArray<TEnumAsByte<EObjectTypeQuery>> Types;
	TArray<AActor*> ActorsToIgnore;// STORES ACTORS TO BE IGNORED BY SPHERE CAST
	TArray<AActor*> OutActors;//STORES THE ACTORS IN THE SPHERE OVERLAP
	TArray<AActor*> FloorArray;//STORES FLOOR ACTOR

	UGameplayStatics::GetAllActorsWithTag(GetWorld(), "Floor", FloorArray);//GETS ALL INSTANCES WITH FLOOR TAG (WILL NEED SIMILAR STATEMENTS FOR OTHER ENVIRONMENT MESHES)

	ActorsToIgnore.Add(GetOwner());
	ActorsToIgnore.Add(ChargeTarget);
	ActorsToIgnore.Add(FloorArray[0]);

	bool isOverlap = UKismetSystemLibrary::SphereOverlapActors(GetWorld(), ChargeTarget->GetActorLocation(), 200, Types, AAI_Parent::StaticClass(), ActorsToIgnore, OutActors);
	DrawDebugSphere(GetWorld(), ChargeTarget->GetActorLocation(), 200, 26, FColor::Green, true, -1);

	//isOverlap IS TRUE IF ANY AI ARE IN THE SPHERE OVERLAP RADIUS
	if (isOverlap)
	{
		for (AActor* Actor : OutActors)
		{
			Cast<AAI_Parent>(Actor)->ApplyDamage(50);
		}
	}
}
*/

/*
void AHEXCharacter::ElfCharge()
{
	FVector SpawnLocation = GetActorRotation().Vector() * 100.f;
	SpawnLocation += GetActorLocation();
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = Instigator;

	ChargeTarget = GetWorld()->SpawnActor<AElfTarget>(ChargeTargetClass, SpawnLocation, GetActorRotation(), SpawnInfo);

	isElfTargeting = true;
}

void AHEXCharacter::ElfChargeEnd()
{
	isElfTargeting = false;
	CastOverlapShere();
	ChargeTarget->Destroy();
}
*/