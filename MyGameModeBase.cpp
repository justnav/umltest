// Fill out your copyright notice in the Description page of Project Settings.

#include "MyGameModeBase.h"
#include "HEXCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "EngineUtils.h"
#include "HEXCharacter.h"
#include "GameFramework/Controller.h"
#include "GameFramework/PlayerStart.h"

AMyGameModeBase::AMyGameModeBase()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/BP_Player"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
		//DefaultPawnClass = NULL;
	}
}


void AMyGameModeBase::BeginPlay()
{
	Super::BeginPlay();
}