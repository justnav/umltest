// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AI_Parent.h"
#include "BaseProjectile.h"
#include "RangedAI.generated.h"

UCLASS()
class HEX_API ARangedAI : public AAI_Parent
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARangedAI();

	UPROPERTY(EditAnywhere, Category = Projectile)
		TSubclassOf<class ABaseProjectile> RangedProjectile;

	virtual void OnPawnSeen(APawn * SeenPawn) override;

	UFUNCTION(BlueprintCallable)
	void ShootProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
