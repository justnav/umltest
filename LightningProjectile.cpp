// Fill out your copyright notice in the Description page of Project Settings.

#include "LightningProjectile.h"
#include "MeleeAI.h"
#include "DrawDebugHelpers.h"
#include "components/SkeletalMeshComponent.h"

// Sets default values
ALightningProjectile::ALightningProjectile()
{
	
	PrimaryActorTick.bCanEverTick = true;

	ShockParticle = CreateDefaultSubobject<UParticleSystem>(TEXT("ShockParticle"));
	//RadialForce = CreateDefaultSubobject<URadialForceComponent>(TEXT("RadialForceComponent"));
	//RadialForce->AddCollisionChannelToAffect(ECC_Pawn);
	//RadialForce->AddCollisionChannelToAffect(ECC_PhysicsBody);
	

}



void ALightningProjectile::BeginPlay()
{
	Super::BeginPlay();
	/**
	RadialForce->ImpulseStrength = -300000.0f;
	RadialForce->Radius = 300.0f; 
	//FCQParams = FCollisionQueryParams::DefaultQueryParam;
	//FCQParams.AddIgnoredActor(this);
	*/
}

/**
void ALightningProjectile::Tick(float DeltaTime)
{
	//Continually fire impulse while projectile is active
	if(bIsActive)
	{
		RadialForce->FireImpulse();
	}
}
*/
void ALightningProjectile::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	/**
	TargetLocation = OtherActor->GetActorLocation();

		//Timer
		GetWorld()->GetTimerManager().SetTimer(_stunTimerHandle, this, &ALightningProjectile::ExplosionEnd, 3.0f, false);

	if (!bHasFired)
	{
		bIsActive = true;
		bHasFired = true;

		//Spawn emitter at hit location
		SpawnedParticle =  UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ShockParticle, TargetLocation);


	}
	*/
}


void ALightningProjectile::ExplosionEnd()
{
	/**
	
	bIsActive = false;

	Destroy(SpawnedParticle);

	//Set Impulse strength to positive value for final explosion
	RadialForce->ImpulseStrength = FinalExplosionForce;

	RadialForce->FireImpulse();
	
	//GetWorld()->GetTimerManager().ClearTimer(_stunTimerHandle );

	Destroy();
	
	*/

}



