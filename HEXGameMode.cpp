// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
#include "HEXGameMode.h"
#include "HEXCharacter.h"
#include "ElfCharacter.h"
#include "WizardCharacter.h"
#include "DwarfCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "EngineUtils.h"
#include "HEXCharacter.h"
#include "GameFramework/Controller.h"
#include "GameFramework/PlayerStart.h"
//#include "EngineUtils.h"
AHEXGameMode::AHEXGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/BP_Player"));
	PlayerSpawn = PlayerPawnBPClass.Class;
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnElfBPClass(TEXT("/Game/Blueprints/BP_PlayerElf"));
	PlayerSecondSpawn = PlayerPawnElfBPClass.Class;
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnWizardBPClass(TEXT("/Game/Blueprints/BP_PlayerWizard"));
	PlayerThirdSpawn = PlayerPawnWizardBPClass.Class;
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnDwarfBPClass(TEXT("/Game/Blueprints/BP_PlayerDwarf"));
	PlayerFourthSpawn = PlayerPawnDwarfBPClass.Class;
	if (PlayerPawnBPClass.Class != NULL)
	{
		//DefaultPawnClass = PlayerPawnBPClass.Class;
		DefaultPawnClass = NULL;
	}
	MaxPlayerCount = 4;
}

void AHEXGameMode::GetPlayerStartPoints()
{
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerStart::StaticClass(), PlayerStartArray);
}
void AHEXGameMode::SpawnPlayers()
{
	for (int32 i = 0; i < MaxPlayerCount; i++)
	{
		Index = i;
		for (int32 y = 0; y < PlayerStartArray.Num(); y++)
		{
			APlayerStart* Player = Cast<APlayerStart>(PlayerStartArray[y]);
			int32 tag = FCString::Atoi(*Player->PlayerStartTag.ToString());

			//SPAWN WIZARD
			if (Index == tag && tag == 1)
			{
				UGameplayStatics::CreatePlayer(GetWorld(), Index);
				APlayerController* controller = UGameplayStatics::GetPlayerController(GetWorld(), Index);
				AWizardCharacter* playercreated = GetWorld()->SpawnActor<AWizardCharacter>(PlayerThirdSpawn, Player->GetActorTransform());
				controller->Possess(playercreated);
				Players.AddUnique(playercreated);
				playercreated->PlayerIndex = Index;
			}
			//SPAWN ELF
			else if (Index == tag && tag == 2)
			{
				UGameplayStatics::CreatePlayer(GetWorld(), Index);
				APlayerController* controller = UGameplayStatics::GetPlayerController(GetWorld(), Index);
				AElfCharacter* playercreated = GetWorld()->SpawnActor<AElfCharacter>(PlayerSecondSpawn, Player->GetActorTransform());
				controller->Possess(playercreated);
				Players.AddUnique(playercreated);
				playercreated->PlayerIndex = Index;
			}
			//SPAWN DWARF
			else if (Index == tag && tag == 3)
			{
				UGameplayStatics::CreatePlayer(GetWorld(), Index);
				APlayerController* controller = UGameplayStatics::GetPlayerController(GetWorld(), Index);
				ADwarfCharacter* playercreated = GetWorld()->SpawnActor<ADwarfCharacter>(PlayerFourthSpawn, Player->GetActorTransform());
				controller->Possess(playercreated);
				Players.AddUnique(playercreated);
				playercreated->PlayerIndex = Index;
			}
			//SPAWN ROGUE
			else if (Index == tag)
			{
				UGameplayStatics::CreatePlayer(GetWorld(), Index);
				APlayerController* controller = UGameplayStatics::GetPlayerController(GetWorld(), Index);
				AHEXCharacter* playercreated = GetWorld()->SpawnActor<AHEXCharacter>(PlayerSpawn, Player->GetActorTransform());
				controller->Possess(playercreated);
				Players.AddUnique(playercreated);
				playercreated->PlayerIndex = Index;
			}
			
		}
	}
}

void AHEXGameMode::BeginPlay()
{
	Super::BeginPlay();
	GetPlayerStartPoints();
	SpawnPlayers();
}