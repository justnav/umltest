// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseProjectile.h"
#include "Engine/GameEngine.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/RadialForceComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/GameEngine.h"
#include "LightningProjectile.generated.h"

UCLASS()
class HEX_API ALightningProjectile : public ABaseProjectile
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ALightningProjectile();




protected:

	UPROPERTY(EditAnywhere)
		UParticleSystem* ShockParticle;

	UPROPERTY(EditAnywhere)
		URadialForceComponent* RadialForce;

	UPROPERTY(EditAnywhere, Category = Projectile)
		int TimesToBounce = 20;

	UPROPERTY(EditAnywhere, Category = Projectile)
		float ExplosionRadius = 500.0f;

	UPROPERTY(EditAnywhere, Category = Projectile)
		float FinalExplosionForce = 300000.0f;

	UPROPERTY(EditAnywhere, Category = Projectile)
		float ExplosionDuration = 5.0f;


	const UObject* TargetEnemy;

	const float InitialZ = GetActorLocation().Z;

	bool bIsActive = false;
	bool bHasFired = false;

	FVector TargetLocation;

	FRotator TargetRotation;

	FTimerHandle _stunTimerHandle;

	UParticleSystemComponent* SpawnedParticle;

	FCollisionQueryParams FCQParams; 

	AActor* HitActor;

public:
	virtual void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit) override;

	virtual void BeginPlay() override;

	//virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void ExplosionEnd();

};


