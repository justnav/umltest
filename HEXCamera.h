// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HEXCamera.generated.h"

UCLASS()
class HEX_API AHEXCamera : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Default, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* SceneRoot;
		/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Default, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

public:	
	// Sets default values for this actor's properties
	AHEXCamera();
	UFUNCTION()
	void ClassSetUp();
	UFUNCTION()
	void UpdateCamera();
	UFUNCTION()
	void SetArmLenght();
	UFUNCTION()
	float CalculateDistance(APawn* Player1, APawn* Player2);
	UFUNCTION()
	FVector GetLocation(APawn* Player);
	UFUNCTION()
	void SetCameraPos();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	TArray<APawn*> PlayersReference;
	TArray<AActor*> PlayerControllers;
	TArray<float> Distances;

	FVector Result;
	FVector TargetPos;
	FVector PreviousPos;
	float inteSpeed;
	APawn* P1;
	APawn* P2;
	APawn* P3;
	APawn* P4;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Heitor)
	float minLength;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Heitor )
	float maxLength;
};
