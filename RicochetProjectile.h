// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"	
#include "GameFramework/Actor.h"
#include "BaseProjectile.h"
#include "Kismet/KismetSystemLibrary.h"
#include "RicochetProjectile.generated.h"

UCLASS()
class HEX_API ARicochetProjectile : public ABaseProjectile
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	ARicochetProjectile();
	//Number of times the projectile jumps
	UPROPERTY(EditAnywhere, Category = Projectile)
	int ProjectileHealth = 5.f;
	FVector TargetLocation;
	AActor* HitActor;
	bool ProjectileHit = false;
	bool ProjectileStop = false;
	FCollisionQueryParams FCQParams;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Projectile)
		float ReboundSpeed = 1000;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Projectile)
		float Radius = 500;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

public:	
	
	virtual void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit) override;
	
	UFUNCTION()
	void OnStop(const FHitResult & Hit);

	UFUNCTION()
	FHitResult GetNextLoc();

	UFUNCTION()
	AActor* FindClosest(TArray<AActor*> List);

	UFUNCTION()
	void CalculateDirection(AActor* OtherActor);

	UFUNCTION()
	void ApplyMovement();

	UFUNCTION()
	void CastOverlapShere();

protected:
//	UFUNCTION(BlueprintNativeEvent)
//void Delay(FVector TargetDir);

};
