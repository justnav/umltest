// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Perception/PawnSensingComponent.h"
#include "AI_Parent.generated.h"

UCLASS()
class HEX_API AAI_Parent : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AAI_Parent();

	UFUNCTION(BlueprintCallable)
		void ApplyDamage(float Damage);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category = "Components")
		UPawnSensingComponent* PawnSensingComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Health")
		float CurrentHealth = 50.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Health")
		float MaxHealth = 50.0f;

	UFUNCTION()
		virtual void OnPawnSeen(APawn *SeenPawn);


public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
