// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DefaultPawn.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "ElfTarget.generated.h"

/**
 * 
 */
UCLASS()
class HEX_API AElfTarget : public ADefaultPawn
{
	GENERATED_BODY()

	//protected:
	//UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Particle)
	//	UParticleSystemComponent* ParticleEffect;

	//public:
	//AElfTarget();
};
