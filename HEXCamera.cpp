// Fill out your copyright notice in the Description page of Project Settings.

#include "HEXCamera.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "EngineUtils.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "HEXGameMode.h"
#include "Runtime/Core/Public/Math/Vector.h"
// Sets default values
AHEXCamera::AHEXCamera()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultSceneRoot"));
	SceneRoot ->SetupAttachment(RootComponent);

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(SceneRoot);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
// Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	 // Camera does not rotate relative to arm


	minLength = 850.0f;
	maxLength = 1100.0f;
	Result = FVector(0.0f, 0.0f, 0.0f);
	TargetPos = FVector(0.0f, 0.0f, 0.0f);
	PreviousPos = FVector(0.0f, 0.0f, 0.0f);
	inteSpeed = 10.0f;
}

void AHEXCamera::ClassSetUp()
{
	AHEXGameMode* myGameMode = (AHEXGameMode *)UGameplayStatics::GetGameMode(GetWorld());
	PlayersReference = myGameMode->Players;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerController::StaticClass(), PlayerControllers);
	for (int32 y = 0; y < PlayerControllers.Num(); y++)
	{
		APlayerController* Player = Cast<APlayerController>(PlayerControllers[y]);
		Player->SetViewTargetWithBlend(this);
	}
	for (int32 i = 0; i < PlayersReference.Num(); i++) {
		APawn* PlayerPawn = PlayersReference[i];
		switch (i)
		{
		case 0:
			P1 = PlayerPawn;
			break;
		case 1:
			P2 = PlayerPawn;
			break;
		case 2:
			P3 = PlayerPawn;
			break;
		case 3:
			P4 = PlayerPawn;
			break;
		default:
			break;
		}
	}
}

void AHEXCamera::UpdateCamera()
{
	SetArmLenght();
	SetCameraPos();
}

void AHEXCamera::SetArmLenght()
{
	Distances.Add(CalculateDistance(P1, P2));
	Distances.Add(CalculateDistance(P1, P3));
	Distances.Add(CalculateDistance(P1, P4));
	Distances.Add(CalculateDistance(P2, P3));
	Distances.Add(CalculateDistance(P2, P4));
	Distances.Add(CalculateDistance(P3, P4));

	CameraBoom->TargetArmLength = FMath::Clamp<float>(FMath::Max<float>(Distances), minLength, maxLength);
	Distances.Empty();
}

float AHEXCamera::CalculateDistance(APawn * Player1, APawn * Player2)
{
	if (Player1 != nullptr && Player2 != nullptr) {
		return Player1->GetDistanceTo(Player2);
	}
	return 0.0f;
}

FVector AHEXCamera::GetLocation(APawn * Player)
{
	if (Player != nullptr) {
		return Player->GetActorLocation();
	}
	return FVector(0.0f, 0.0f, 0.0f);
}

void AHEXCamera::SetCameraPos()
{
	//Result += P1->GetActorLocation();
	//Result += P2->GetActorLocation();
	//Result += P3->GetActorLocation();
	//Result += P4->GetActorLocation();
	//Result /= 4.0f;
	//TargetPos = Result;

	this->SetActorLocation(((P1->GetActorLocation() + P2->GetActorLocation() + P2->GetActorLocation() + P3->GetActorLocation()))/4.0);
	//this->SetActorLocation(FMath::VInterpTo(PreviousPos, TargetPos, UGameplayStatics::GetWorldDeltaSeconds(GetWorld()), inteSpeed));
	//PreviousPos = this->GetActorLocation();
}

// Called when the game starts or when spawned
void AHEXCamera::BeginPlay()
{
	Super::BeginPlay();
	FTimerDelegate TimerDel;
	FTimerHandle TimerHandle;
	FTimerHandle TimerHandle1;
	//Binding the function with specific variables
	TimerDel.BindUFunction(this, FName("ClassSetUp"));
	GetWorldTimerManager().SetTimer(TimerHandle, TimerDel, 0.0001f, false);


	TimerDel.BindUFunction(this, FName("UpdateCamera"));
	//Calling MyUsefulFunction after 5 seconds without looping
	GetWorldTimerManager().SetTimer(TimerHandle1, TimerDel, 0.001f, true);
}

// Called every frame
void AHEXCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

